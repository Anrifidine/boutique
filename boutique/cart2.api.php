<?php
include './connexion.php';
session_start();

$method = $_SERVER["REQUEST_METHOD"];

if ($method == "GET") {
    echo json_encode(["session" => $_SESSION['user_id']]);
}

if ($method == 'POST') {
    $operation = "";
    $data = json_decode(file_get_contents("php://input"), true);

    if (isset($data['id']) && isset($data['quantity'])) {
        $product_id = $data['id'];
        $quantity = $data['quantity'];
        $user_id = $_SESSION['user_id'];

        if ($user_id) {
            try {
                // Escape data to prevent SQL injection
                $user_id = mysqli_real_escape_string($conn, $user_id);
                $product_id = mysqli_real_escape_string($conn, $product_id);

                // Check if the product is already in the cart
                $select_query = "SELECT * FROM produitcart WHERE id_user=$user_id AND id_product=$product_id";
                $result = mysqli_query($conn, $select_query);

                if (mysqli_num_rows($result) > 0) {
                    $operation = "delete";

                    // Update the quantity in the existing cart entry
                    $update_query = "UPDATE produitcart SET quantite=quantite-$quantity WHERE id_user=$user_id AND id_product=$product_id";
                    $result = mysqli_query($conn, $update_query);

                    // Remove the entry if the quantity becomes 0 or less
                    $delete_query = "DELETE FROM produitcart WHERE id_user=$user_id AND id_product=$product_id AND quantite <= 0";
                    mysqli_query($conn, $delete_query);
                } else {
                    $operation = "post";

                    // Insert the product into the cart with the specified quantity
                    $insert_query = "INSERT INTO produitcart (id_user, id_product, quantite) VALUES ('$user_id', '$product_id', '$quantity')";
                    $result = mysqli_query($conn, $insert_query);
                }

                // Check if the operation was successful
                if ($result) {
                    echo json_encode([
                        'status' => 'success', 'message' => 'Product updated in cart', 'operation' => $operation
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'error', 'message' => 'Failed to update product in cart', 'user' => $user_id, 'product' => $product_id
                    ]);
                }
            } catch (Exception $e) {
                echo json_encode(['status' => 'error', 'message' => 'Database error']);
            } finally {
                mysqli_close($conn);
            }
        } else {
            echo json_encode(['status' => 'error', 'message' => 'User not logged in']);
        }
    } else {
        echo json_encode(['status' => 'error', 'message' => 'Invalid data']);
    }
} else {
    echo json_encode(['status' => 'error', 'message' => 'Invalid request method']);
}
