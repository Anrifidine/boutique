<?php

include 'connexion.php';
session_start();
$admin_id = $_SESSION['admin_name'];
if (!isset($admin_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}
if (isset($_POST['add_product'])) {
    $product_name = mysqli_real_escape_string($conn, $_POST['name']);
    $product_price = mysqli_real_escape_string($conn, $_POST['price']);
    $product_detail = mysqli_real_escape_string($conn, $_POST['detail']);
    $image = $_FILES['image']['name'];
    $image_size = $_FILES['image']['size'];
    $image_tmp_name = $_FILES['image']['tmp_name'];
    $image_folder = 'image/' . $image;

    $select_product_name = mysqli_query($conn, "SELECT name FROM `products` WHERE name = '$product_name'") or die('query faild');
    if (mysqli_num_rows($select_product_name) > 0) {
        $message[] = 'Le nom de produit exsiste deja';
    } else {
        $insert_product = mysqli_query($conn, "INSERT INTO `products` (`name`, `price`, `product_detail`, `image`) VALUES ('$product_name', '$product_price', '$product_detail', '$image')") or die('query failed');
        if ($insert_product) {
            if ($image_size > 2000000) {
                $message[] = "l'image est trop grande";
            } else {
                move_uploaded_file($image_tmp_name, $image_folder);
                $message[] = 'produit ajouter avec succes';
            }
        }
    }
}
if (isset($_GET['delete'])) {
    $delete_id = $_GET['delete'];

    mysqli_query($conn, "DELETE FROM `users` WHERE id = '$delete_id'") or die('query failed3');
    $message[] = 'utilisateur supprimer avec succes';

    header('location:admin_user.php');
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css">
    <link rel="stylesheet" href="style.css">
    <title>admin pannel</title>
</head>

<body>
    <?php include 'admin_header.php'; ?>
    <?php
    if (isset($message)) {
        foreach ($message as $message) {
            echo '<div class="message">
                            <span>' . $message . '</span>
                            <i class="bi bi-x-circle" onclick="this.parentElement.remove()"><span class="material-icons">
                            clear
                            </span></i>
                        </div>';
        }
    }
    ?>

    <section class="message-container">
        <h1 class="title">compte utilisateur</h1>
        <div class="box-container">
            <?php
            $select_users = mysqli_query($conn, "SELECT * FROM `users`") or die('query failed');
            if (mysqli_num_rows($select_users) > 0) {
                while ($fetch_users = mysqli_fetch_assoc($select_users)) {
            ?>
                    <div class="box">
                        <p>user id: <span><?php echo $fetch_users['id']; ?></span></p>
                        <p>name: <span><?php echo $fetch_users['name']; ?></span></p>
                        <p>email: <span><?php echo $fetch_users['email']; ?></span></p>
                        <p>type d'utilisateur: <span style="color: <?php if ($fetch_users['user_type'] == 'admin') {
                                                                        echo 'red';
                                                                    }; ?>;"><?php echo $fetch_users['user_type'] ?></span></p>
                        <a href="admin_user.php?delete=<?php echo $fetch_users['id']; ?>;" class="delete" onclick="return confirm('supprimer cette utilisateur');">supprimer</a>



                    </div>
            <?php
                }
            } else {
                echo '<div class="empty">
                        <p>pas de produit ajouté</p>
                        </div>';
            }

            ?>

        </div>

    </section>


    <script type="text/javasript" src="script.js"></script>

</body>

</html>