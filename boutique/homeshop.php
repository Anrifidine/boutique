<?php

include 'connexion.php';
session_start();


$user_id = $_SESSION['user_id'];
if (!isset($user_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}
function isProductInWishlist($user_id, $product_id)
{
    global $conn;

    $user_id = mysqli_real_escape_string($conn, $user_id);
    $product_id = mysqli_real_escape_string($conn, $product_id);

    $select_query = "SELECT * FROM productsinwishlist WHERE id_user=$user_id AND id_product=$product_id";
    $result = mysqli_query($conn, $select_query);

    return mysqli_num_rows($result) > 0;
}
function isProductInCart($user_id, $product_id)
{
    global $conn;

    $user_id = mysqli_real_escape_string($conn, $user_id);
    $product_id = mysqli_real_escape_string($conn, $product_id);

    $select_query = "SELECT * FROM produitcart WHERE id_user=$user_id AND id_product=$product_id";
    $result = mysqli_query($conn, $select_query);

    return mysqli_num_rows($result) > 0;
}
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.17.0/font/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="main.css">

    <title>Document</title>
</head>

<body>
    <?php include 'header.php'; ?>

    <div class="vide"></div>
    <div class="box-container show-products test">
        <?php
        $select_products = mysqli_query($conn, "SELECT * FROM `products`") or die('query failed');
        if (mysqli_num_rows($select_products) > 0) {
            while ($fetch_products = mysqli_fetch_assoc($select_products)) {
                $product_id = $fetch_products['id'];
                $is_in_wishlist = isProductInWishlist($user_id, $product_id);
                $is_in_cart = isProductInCart($user_id, $product_id);

        ?>
                <form method="post">
                    <div class="box first">
                        <img src="image/<?php echo $fetch_products['image']; ?>">
                        <p>prix :<?php echo $fetch_products['price']; ?>$ </p>
                        <h4><?php echo $fetch_products['name']; ?></h4>
                        <input type="hidden" name="product_id" value="<?php echo $fetch_products['id']; ?>">
                        <div class="ico"><a href="view_page.php?pid=<?php echo $fetch_products['id'] ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                    <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                    <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                </svg></a>

                            <button id="<?php echo $product_id . 'Love'; ?>" onclick="addToWishList(<?php echo $product_id; ?>)" type="button" name="add_to_wishlist" style="<?php echo $is_in_wishlist ? 'background-color: red;' : ''; ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
                                </svg>
                            </button>
                            <button id="<?php $rescart = $fetch_products['id'] . 'cart';
                                        echo $rescart; ?>" onclick="addToCart(<?php echo $fetch_products['id']; ?>)" type="button" name="add_to_box-container" style="<?php echo $is_in_cart ? 'background-color: yellow;' : ''; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-bag" viewBox="0 0 16 16">
                                    <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z" />
                                </svg></button>
                        </div>
                    </div>
                </form>

        <?php
            }
        } else {
            echo '<div class="empty">
        <p>pas de produit ajouté</p>
        </div>';
        }
        ?>

    </div>









    <script src="script2.js"></script>

    <?php include 'footer.php'; ?>

</body>

</html>