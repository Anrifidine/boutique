<?php

include 'connexion.php';
session_start();
$admin_id = $_SESSION['admin_name'];
if (!isset($admin_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}
if (isset($_POST['add_product'])) {
    $product_name = mysqli_real_escape_string($conn, $_POST['name']);
    $product_price = mysqli_real_escape_string($conn, $_POST['price']);
    $product_detail = mysqli_real_escape_string($conn, $_POST['detail']);
    $image = $_FILES['image']['name'];
    $image_size = $_FILES['image']['size'];
    $image_tmp_name = $_FILES['image']['tmp_name'];
    $image_folder = 'image/' . $image;

    $select_product_name = mysqli_query($conn, "SELECT name FROM `products` WHERE name = '$product_name'") or die('query faild');
    if (mysqli_num_rows($select_product_name) > 0) {
        $message[] = 'Le nom de produit exsiste deja';
    } else {
        $insert_product = mysqli_query($conn, "INSERT INTO `products` (`name`, `price`, `product_detail`, `image`) VALUES ('$product_name', '$product_price', '$product_detail', '$image')") or die('query failed');
        if ($insert_product) {
            if ($image_size > 2000000) {
                $message[] = "l'image est trop grande";
            } else {
                move_uploaded_file($image_tmp_name, $image_folder);
                $message[] = 'produit ajouter avec succes';
            }
        }
    }
}
if (isset($_GET['delete'])) {
    $delete_id = $_GET['delete'];

    mysqli_query($conn, "DELETE FROM `order` WHERE id = '$delete_id'") or die('query failed3');
    $message[] = 'utilisateur supprimer avec succes';

    header('location:admin_orders.php');
}
if (isset($_POST['update_order'])) {
    $order_id = $_POST['order_id'];
    $update_payment = $_POST['update_payment'];
    mysqli_query($conn, "UPDATE `order` SET payment_status = '$update_payment' WHERE id='$order_id'") or die('query failed');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css">
    <link rel="stylesheet" href="style.css">
    <title>admin pannel</title>
</head>

<body>
    <?php include 'admin_header.php'; ?>
    <?php
    if (isset($message)) {
        foreach ($message as $message) {
            echo '<div class="message">
                            <span>' . $message . '</span>
                            <i class="bi bi-x-circle" onclick="this.parentElement.remove()"><span class="material-icons">
                            clear
                            </span></i>
                        </div>';
        }
    }
    ?>

    <section class="order-container">
        <h1 class="title">totalite des commandes</h1>
        <div class="box-container">
            <?php
            $select_orders = mysqli_query($conn, "SELECT * FROM `order`") or die('query failed');
            if (mysqli_num_rows($select_orders) > 0) {
                while ($fetch_orders = mysqli_fetch_assoc($select_orders)) {
            ?>
                    <div class="box">
                        <p>user name: <span><?php echo $fetch_orders['name']; ?></span></p>
                        <p>user id: <span><?php echo $fetch_orders['user_id']; ?></span></p>
                        <p>placer le: <span><?php echo $fetch_orders['placed_on']; ?></span></p>
                        <p>number: <span><?php echo $fetch_orders['number'] ?></span></p>
                        <p>email: <span><?php echo $fetch_orders['email'] ?></span></p>
                        <p>prix total:<span> <?php echo $fetch_orders['total_price'] ?></span></p>
                        <p>methode: <span><?php echo $fetch_orders['method'] ?></span></p>
                        <p>address: <span><?php echo $fetch_orders['address'] ?></span></p>
                        <p>produit total:<span> <?php echo $fetch_orders['total_products'] ?></span></p>
                        <form method="post">
                            <input type="hidden" name="order_id" value="<?php echo $fetch_orders['id'] ?>">
                            <select name="update_payment">
                                <option <?php if ($fetch_orders['payment_status'] === 'pending') echo 'selected'; ?> value="pending">en attente</option>
                                <option <?php if ($fetch_orders['payment_status'] === 'complete') echo 'selected'; ?> value="complete">complete</option>
                            </select>
                            <input type="submit" name="update_order" value="update payment" class="btn">
                            <a href="admin_orders.php?delete=<?php echo $fetch_orders['id']; ?>;" class="delete" onclick="return confirm('supprimer cette utilisateur');">supprimer</a>

                        </form>







                    </div>
            <?php
                }
            } else {
                echo '<div class="empty">
                        <p>pas de commande ajouté</p>
                        </div>';
            }

            ?>

        </div>

    </section>


    <script type="text/javasript" src="script.js"></script>

</body>

</html>