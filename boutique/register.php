<?php
include 'connexion.php';
if (isset($_POST['submit-btn'])) {
    $filter_name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
    $name = mysqli_real_escape_string($conn, $filter_name);
    $filter_email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
    $email = mysqli_real_escape_string($conn, $filter_email);
    $filter_password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
    $password = mysqli_real_escape_string($conn, $filter_password);
    $filter_cpassword = filter_var($_POST['cpassword'], FILTER_SANITIZE_STRING);
    $cpassword = mysqli_real_escape_string($conn, $filter_cpassword);
    $select_user = mysqli_query($conn, "SELECT * FROM `users` WHERE email = '$email'") or die('query failed');
    if (mysqli_num_rows($select_user) > 0) {
        $message[] = "l'utilisateur existe deja";
    } else {
        if ($password != $cpassword) {
            $message[] = 'mauvais mot de passe';
        } else {
            mysqli_query($conn, "INSERT INTO `users`(`name`,`email`,`password`) value ('$name','$email','$password')") or die('query file');
            $message[] = 'inscription reussit';
            header('localosation:login.php');
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="style.css">
    <title>Register page</title>
</head>

<body>


    <section class="form-container">
        <?php
        if (isset($message)) {
            foreach ($message as $message) {
                echo '<div class="message">
                            <span>' . $message . '</span>
                            <i class="bi bi-x-circle" onclick="this.parentElement.remove()"><span class="material-icons">
                            clear
                            </span></i>
                        </div>';
            }
        }
        ?>
        <form method="post">
            <h1>S'inscrire maintenant</h1>
            <input type="text" name="name" placeholder="entrez votre nom" required>
            <input type="email" name="email" placeholder="entrez votre email" required>
            <input type="password" name="password" placeholder="entrez votre mot de pass" required>
            <input type="password" name="cpassword" placeholder="confirmez votre mot de pass" required>
            <input type="submit" name="submit-btn" value="S'inscrire " class="btn">
            <p>vous avez deja un compte ?<a href="login.php">connexion</a></p>


        </form>

    </section>

</body>

</html>