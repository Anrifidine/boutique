<?php

include 'connexion.php';
session_start();
$admin_id = $_SESSION['admin_name'];
if (!isset($admin_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}
if (isset($_POST['add_product'])) {
    // Sanitize input data
    $product_name = mysqli_real_escape_string($conn, $_POST['name']);
    $product_price = mysqli_real_escape_string($conn, $_POST['price']);
    $product_detail = mysqli_real_escape_string($conn, $_POST['detail']);

    // Check if the product name already exists
    $select_product_name = mysqli_query($conn, "SELECT name FROM `products` WHERE name = '$product_name'") or die(mysqli_error($conn));
    if (mysqli_num_rows($select_product_name) > 0) {
        $message[] = 'Le nom du produit existe déjà';
    } else {
        // Handle the main product image
        $main_image = $_FILES['image'];
        $main_image_name = $main_image['name'];
        $main_image_tmp_name = $main_image['tmp_name'];
        $main_image_folder = 'image/' . $main_image_name;

        // Check if the main image size is within the limit
        if ($main_image['size'] > 2000000) {
            $message[] = "L'image principale est trop grande";
        } else {
            // Move the uploaded main image to the specified folder
            move_uploaded_file($main_image_tmp_name, $main_image_folder);

            // Insert product details and main image into the database
            $insert_product = mysqli_query($conn, "INSERT INTO `products` (`name`, `price`, `product_detail`, `image`) VALUES ('$product_name', '$product_price', '$product_detail', '$main_image_name')") or die(mysqli_error($conn));

            if ($insert_product) {
                $product_id = mysqli_insert_id($conn);
                $images = $_FILES['images'];

                // Iterate through the uploaded secondary images
                for ($i = 0; $i < count($images['name']); $i++) {
                    $image = $images['name'][$i];
                    $image_size = $images['size'][$i];
                    $image_tmp_name = $images['tmp_name'][$i];
                    $image_folder = 'image/' . $image;

                    // Check if the secondary image size is within the limit
                    if ($image_size > 2000000) {
                        $message[] = "L'image secondaire est trop grande";
                    } else {
                        // Move the uploaded secondary image to the specified folder
                        move_uploaded_file($image_tmp_name, $image_folder);

                        // Insert secondary image details into the 'product_images' table
                        $insert_image = mysqli_query($conn, "INSERT INTO `product_images` (`product_id`, `image`) VALUES ('$product_id', '$image')") or die(mysqli_error($conn));


                        $message[] = 'Produit ajouté avec succès';
                    }
                }
            }
        }
    }
}
if (isset($_GET['delete'])) {
    $delete_id = $_GET['delete'];

    // Get the names of all images associated with the product
    $select_images = mysqli_query($conn, "SELECT image FROM `product_images` WHERE product_id = '$delete_id'") or die("La requête a échoué : " . mysqli_error($conn));

    // Loop through each image and delete it if it is not used by any other product
    while ($fetch_image = mysqli_fetch_assoc($select_images)) {
        $image_name = $fetch_image['image'];

        // Check if the image is used by other products
        $image_usage_query = mysqli_query($conn, "SELECT id FROM `product_images` WHERE image = '$image_name' AND product_id != '$delete_id'");

        if (!$image_usage_query || mysqli_num_rows($image_usage_query) == 0) {
            // Image is not used by any other product, delete it
            if (isset($image_name) && $image_name !== null) {
                // Check if the image file exists before attempting to delete
                if (file_exists('image/' . $image_name)) {
                    unlink('image/' . $image_name);
                }
            }
        }
    }

    // Delete the main product image if it exists
    $select_delete_image = mysqli_query($conn, "SELECT image FROM `products` WHERE id = '$delete_id'") or die("La requête a échoué : " . mysqli_error($conn));
    $fetch_delete_image = mysqli_fetch_assoc($select_delete_image);
    if (isset($fetch_delete_image['image']) && $fetch_delete_image['image'] !== null) {
        unlink('image/' . $fetch_delete_image['image']);
    }

    // Delete the product and related data
    mysqli_query($conn, "DELETE FROM `products` WHERE id = '$delete_id'") or die('query failed1');
    mysqli_query($conn, "DELETE FROM `cart` WHERE pid = '$delete_id'") or die("La requête a échoué : " . mysqli_error($conn));
    mysqli_query($conn, "DELETE FROM `wishlist` WHERE pid = '$delete_id'") or die('query failed3');
    mysqli_query($conn, "DELETE FROM `product_images` WHERE product_id = '$delete_id'") or die('query failed3');

    header('location:admin_product.php');
}

if (isset($_GET['delete_image'])) {
    $delete_image_id = $_GET['delete_image'];

    // Valider et sécuriser $delete_image_id pour éviter les injections SQL

    // Récupérez le nom du fichier de l'image avant de la supprimer
    $image_query = mysqli_query($conn, "SELECT image, product_id FROM product_images WHERE id = '$delete_image_id'");
    $image_row = mysqli_fetch_assoc($image_query);
    $image_name = $image_row['image'];
    $product_id = $image_row['product_id'];

    // Supprimez l'enregistrement de la base de données
    mysqli_query($conn, "DELETE FROM `product_images` WHERE id = '$delete_image_id'");

    // Vérifiez si l'image est utilisée par d'autres produits dans la table product_images
    $image_usage_query = mysqli_query($conn, "SELECT id FROM `product_images` WHERE image = '$image_name' AND product_id != '$product_id'");

    if (!$image_usage_query || mysqli_num_rows($image_usage_query) == 0) {
        // Vérifiez si l'image est utilisée par d'autres produits dans la table products
        $product_image_usage_query = mysqli_query($conn, "SELECT id FROM `products` WHERE image = '$image_name' AND id != '$product_id'");

        if (!$product_image_usage_query || mysqli_num_rows($product_image_usage_query) == 0) {
            // L'image n'est pas utilisée par d'autres produits, supprimez le fichier image
            if (file_exists('image/' . $image_name)) {
                unlink('image/' . $image_name);
            }
        }
    }

    // Redirigez l'utilisateur vers la page d'édition
    header("Location: admin_product.php?");
    exit();
}

if (isset($_POST['updte_product'])) {
    // Sanitize input data
    $update_id = $_POST['update_id'];
    $update_name = mysqli_real_escape_string($conn, $_POST['update_name']);
    $update_price = mysqli_real_escape_string($conn, $_POST['update_price']);
    $update_detail = mysqli_real_escape_string($conn, $_POST['update_detail']);

    // Check if a new main image is provided
    if ($_FILES['update_image']['size'] > 0) {
        $update_image = $_FILES['update_image']['name'];
        $update_image_tmp_name = $_FILES['update_image']['tmp_name'];
        $update_image_folder = 'image/' . $update_image;

        // Move the uploaded main image to the specified folder
        move_uploaded_file($update_image_tmp_name, $update_image_folder);

        // Update product data with the new main image
        $update_query = mysqli_query($conn, "UPDATE `products` SET `name`='$update_name', `price`='$update_price', `product_detail`='$update_detail', `image`='$update_image' WHERE id = '$update_id'") or die('Query failed');

        if (!$update_query) {
            die('Query failed');
        }
    } else {
        // No new main image provided, update only text data
        $update_query = mysqli_query($conn, "UPDATE `products` SET `name`='$update_name', `price`='$update_price', `product_detail`='$update_detail' WHERE id = '$update_id'") or die('Query failed');

        if (!$update_query) {
            die('Query failed');
        }
    }

    // Check if additional images are provided
    if (!empty($_FILES['add_images']['name'][0])) {
        $images = $_FILES['add_images'];

        // Iterate through the uploaded additional images
        for ($i = 0; $i < count($images['name']); $i++) {
            $image = $images['name'][$i];
            $image_size = $images['size'][$i];
            $image_tmp_name = $images['tmp_name'][$i];
            $image_folder = 'image/' . $image;

            // Check if the additional image size is within the limit
            if ($image_size > 2000000) {
                die("L'image secondaire est trop grande");
            }

            // Move the uploaded additional image to the specified folder
            move_uploaded_file($image_tmp_name, $image_folder);

            // Insert the additional image details into the 'product_images' table
            $insert_image = mysqli_query($conn, "INSERT INTO `product_images` (`product_id`, `image`) VALUES ('$update_id', '$image')") or die('Query failed');

            if (!$insert_image) {
                die('Query failed');
            }
        }
    }

    // Redirect the user to the admin_product.php page
    header('location: admin_product.php');
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css">
    <link rel="stylesheet" href="style.css">
    <title>admin pannel</title>
</head>

<body>
    <?php include 'admin_header.php'; ?>
    <?php
    if (isset($message)) {
        foreach ($message as $msg) {
            echo '<div class="message">
                    <span>' . $msg . '</span>
                    <i class="bi bi-x-circle" onclick="this.parentElement.remove()"><span class="material-icons">clear</span></i>
                  </div>';
        }
    }
    ?>

    <section class="add-products form-container">
        <form method="POST" action="" enctype="multipart/form-data">
            <div class="input-field">
                <label> product name</label>
                <input type="text" name="name" required>
            </div>
            <div class="input-field">
                <label> product price</label>
                <input type="text" name="price" required>
            </div>
            <div class="input-field">
                <label> product detail</label>
                <textarea name="detail" required></textarea>
            </div>
            <div class="input-field">
                <label>image principale</label>
                <input type="file" name="image" accept="image/jpg, image/jpeg, image/png, image/webp" required>
            </div>
            <div class="input-field">
                <label>images secondaire</label>
                <input type="file" name="images[]" accept="image/jpg, image/jpeg, image/png, image/webp" multiple required>
            </div>
            <input type="submit" name="add_product" value="add product" class="btn">
        </form>
    </section>

    <div class="line4"></div>
    <div class="box-container show-products">
        <?php
        $select_products = mysqli_query($conn, "SELECT * FROM products") or die('query failed');

        if (mysqli_num_rows($select_products) > 0) {
            while ($fetch_products = mysqli_fetch_assoc($select_products)) {
        ?>
                <div class="box">
                    <img src="image/<?php echo $fetch_products['image']; ?>">
                    <?php
                    $product_id = $fetch_products['id'];
                    $select_images = mysqli_query($conn, "SELECT id, image FROM product_images WHERE product_id = $product_id") or die('query failed');

                    while ($fetch_images = mysqli_fetch_assoc($select_images)) {
                    ?>
                        <a href="#" onclick="deleteImage(<?php echo $fetch_images['id']; ?>)">
                            <img src="image/<?php echo $fetch_images['image']; ?>" class="small-image">
                        </a>
                    <?php
                    }
                    ?>
                    <p>prix :<?php echo $fetch_products['price']; ?>$ </p>
                    <h4><?php echo $fetch_products['name']; ?></h4>
                    <details><?php echo $fetch_products['product_detail']; ?></details>
                    <a href="admin_product.php?edit=<?php echo $fetch_products['id']; ?>" class="edit">edit</a>
                    <a href="admin_product.php?delete=<?php echo $fetch_products['id']; ?>" class="delete" onclick="return confirm('Veux-tu vraiment enlever ce produit ?')">delete</a>
                </div>
        <?php
            }
        } else {
            echo '<div class="empty">
            <p>Pas de produit ajouté</p>
          </div>';
        }
        ?>
    </div>


    <section class="update-container">
        <?php
        if (isset($_GET['edit'])) {
            $edit_id = $_GET['edit'];
            $edit_query = mysqli_query($conn, "SELECT * FROM `products` WHERE id='$edit_id'") or die('query failed');
            if (mysqli_num_rows($edit_query) > 0) {
                while ($fetch_edit = mysqli_fetch_assoc($edit_query)) {
        ?>
                    <form method="POST" enctype="multipart/form-data">
                        <img src="image/<?php echo $fetch_edit['image']; ?>">
                        <?php
                        $product_id = $fetch_edit['id'];
                        $select_images = mysqli_query($conn, "SELECT id, image FROM product_images WHERE product_id = $product_id") or die('query failed');

                        while ($fetch_images = mysqli_fetch_assoc($select_images)) {
                        ?>
                            <img src="image/<?php echo $fetch_images['image']; ?>" class="small-image">
                        <?php
                        }
                        ?>

                        <input type="hidden" name="update_id" value="<?php echo $fetch_edit['id']; ?>">
                        <input type="text" name="update_name" value="<?php echo $fetch_edit['name']; ?>">
                        <input type="number" name="update_price" min="0" value="<?php echo $fetch_edit['price']; ?>">
                        <textarea name="update_detail"><?php echo $fetch_edit['product_detail']; ?></textarea>
                        <label for="">Changer la photo principal</label>
                        <input type="file" name="update_image" accept="image/jpg, image/jpeg, image/png, image/webp">
                        <label for="">ajoutez des photos secondaire</label>
                        <input type="file" name="add_images[]" accept="image/jpg, image/jpeg, image/png, image/webp" multiple>
                        <input type="submit" name="updte_product" value="update" class="edit">
                        <input type="reset" name="" value="cancle" class="option-btn btn" id="close-form">
                    </form>
        <?php
                }
            }
            echo "<script>document.querySelector('.update-container').style.display='block'</script>";
        }
        ?>
    </section>

    <script type="text/javascript" src="script.js"></script>
    <script>
        function deleteImage(imageId) {
            var confirmDelete = confirm('Veux-tu vraiment supprimer cette image ?');
            if (confirmDelete) {
                window.location.href = 'admin_product.php?delete_image=' + imageId;
            }
        }
    </script>

</body>

</html>