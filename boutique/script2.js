const header = document.querySelector("header");
function fixedNavbar() {
  header.classList.toggle("scrolled", window.pageYOffset > 0);
}
fixedNavbar();
window.addEventListener("scroll", fixedNavbar);

let menu = document.querySelector("#menu-btn");

menu.addEventListener("click", function () {
  let nav = document.querySelector(".navbar");
  nav.classList.toggle("active");
});

// Sélectionnez l'icône avec l'ID user-btn
var userBtn = document.getElementById("user-btn");

// Sélectionnez la div avec l'ID user-box
var userBox = document.getElementById("user-box");

// Rendre la div user-box initialement invisible
userBox.style.display = "none";

// Ajoutez un gestionnaire d'événements au clic sur l'icône user-btn
userBtn.addEventListener("click", function () {
  // Vérifiez si la div user-box est actuellement visible
  if (userBox.style.display === "none" || userBox.style.display === "") {
    // Si elle est cachée, montrez-la
    userBox.style.display = "block";
  } else {
    // Sinon, cachez-la
    userBox.style.display = "none";
  }
});
document.addEventListener("DOMContentLoaded", function () {
  // Sélectionnez le bouton "Cancel" par son ID
  const cancelButton = document.getElementById("close-form");

  // Ajoutez un gestionnaire d'événement de clic au bouton "Cancel"
  cancelButton.addEventListener("click", function () {
    // Cachez le conteneur de mise à jour lorsque le bouton "Cancel" est cliqué
    document.querySelector(".update-container").style.display = "none";
  });
});
window.onbeforeunload = function () {
  return null;
};
async function addToWishList(id) {
  let response = await fetch("http://localhost:3000/like.api.php", {
    method: "POST",
    body: JSON.stringify({
      id: id,
    }),
  });
  let json = await response.json();
  console.log(json);
  if (json.operation == "delet") {
    if (document.getElementById(id + "Love") != null) {
      let coeur = document.getElementById(id + "Love");

      coeur.style.backgroundColor = "white";
    }
  } else {
    if (document.getElementById(id + "Love") != null) {
      let coeur = document.getElementById(id + "Love");

      coeur.style.backgroundColor = "red";
    }
  }
}
async function addToCart(id) {
  let response = await fetch("http://localhost:3000/cart.api.php", {
    method: "POST",
    body: JSON.stringify({
      id: id,
    }),
  });
  let json = await response.json();
  console.log(json);
  if (json.operation == "delet") {
    if (document.getElementById(id + "cart") != null) {
      let coeur = document.getElementById(id + "cart");

      coeur.style.backgroundColor = "white";
    }
  } else {
    if (document.getElementById(id + "cart") != null) {
      let coeur = document.getElementById(id + "cart");

      coeur.style.backgroundColor = "yellow";
    }
  }
}
async function addToCartView(id) {
  // Fetch the product quantity from the input field
  let quantity = document.getElementById("product_quantity_" + id).value;

  let response = await fetch("http://localhost:3000/cart2.api.php", {
    method: "POST",
    body: JSON.stringify({
      id: id,
      quantity: quantity,
    }),
  });

  let json = await response.json();
  console.log(json);

  if (json.operation === "post") {
    // Changement de couleur en jaune si l'opération est "post"
    if (document.getElementById(id + "cart") != null) {
      let coeur = document.getElementById(id + "cart");
      coeur.style.backgroundColor = "yellow";
    }
  } else {
    // Changement de couleur en blanc si l'opération n'est pas "post" (peut être "delet" ou autre chose)
    if (document.getElementById(id + "cart") != null) {
      let coeur = document.getElementById(id + "cart");
      coeur.style.backgroundColor = "white";
    }
  }
}
