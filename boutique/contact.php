<?php
include 'connexion.php';

session_start();
$user_id = $_SESSION['user_id'];
if (!isset($user_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}


if (isset($_POST['submit-btn'])) {
    // Récupérer les données du formulaire
    $name = isset($_POST['name']) ? $_POST['name'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
    $message = mysqli_real_escape_string($conn, $_POST['message']);

    // Insérez le message dans la base de données
    $insert_query = mysqli_query($conn, "INSERT INTO `message` (`user_id`, `name`, `email`, `number`, `message`) VALUES ('$user_id', '$name', '$email', '$phone', '$message')");

    if ($insert_query) {
        $messages[] = "Message envoyé avec succès";
    } else {
        $messages[] = "Échec de l'envoi du message. Veuillez réessayer.";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.17.0/font/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="contact.css">
    <title>Détails du Produit</title>
</head>

<body>
    <?php include 'header.php'; ?>

    <div class="vide"></div>

    <section class="form-container">
        <?php
        if (isset($messages)) {
            foreach ($messages as $messages) {
                echo '<div class="message">
                            <span>' . $messages . '</span>
                            <i class="bi bi-x-circle" onclick="this.parentElement.remove()"><span class="material-icons">
                            X
                            </span></i>
                        </div>';
            }
        }
        ?>

        <form method="post" action=""> <!-- Remplacez "traitement.php" par le nom de votre script de traitement -->
            <h1>contact nous</h1>
            <div class="input-field">
                <label for="phone">Numéro de téléphone (facultatif):</label>
                <input type="tel" id="phone" name="phone" placeholder="Entrez votre numéro de téléphone">
            </div>
            <div class="input-field">
                <label for="message">Message:</label>
                <textarea id="message" name="message" rows="4" placeholder="Entrez votre message ici" required></textarea>
            </div>
            <?php
            $user = mysqli_query($conn, "SELECT * FROM `order`") or die('query failed');
            if (mysqli_num_rows($user) > 0) {
                while ($fetch_user = mysqli_fetch_assoc($user)) {
            ?>
                    <input type="hidden" id="email" name="email" value="<?php echo $fetch_user['email'] ?>">

                    <input type="hidden" id="name" name="name" value="<?php echo $fetch_user['name'] ?>">
            <?php }
            } ?>

            <input type="submit" name="submit-btn" value="envoyer" class="btn">

        </form>
    </section>





    <?php include 'footer.php'; ?>

</body>

</html>