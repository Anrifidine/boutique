const header = document.querySelector("header");
function fixedNavbar() {
  header.classList.toggle("scrolled", window.pageYOffset > 0);
}
fixedNavbar();
window.addEventListener("scroll", fixedNavbar);

let menu = document.querySelector("#menu-btn");

menu.addEventListener("click", function () {
  let nav = document.querySelector(".navbar");
  nav.classList.toggle("active");
});

// Sélectionnez l'icône avec l'ID user-btn
var userBtn = document.getElementById("user-btn");

// Sélectionnez la div avec l'ID user-box
var userBox = document.getElementById("user-box");

// Rendre la div user-box initialement invisible
userBox.style.display = "none";

// Ajoutez un gestionnaire d'événements au clic sur l'icône user-btn
userBtn.addEventListener("click", function () {
  // Vérifiez si la div user-box est actuellement visible
  if (userBox.style.display === "none" || userBox.style.display === "") {
    // Si elle est cachée, montrez-la
    userBox.style.display = "block";
  } else {
    // Sinon, cachez-la
    userBox.style.display = "none";
  }
});
document.addEventListener("DOMContentLoaded", function () {
  // Sélectionnez le bouton "Cancel" par son ID
  const cancelButton = document.getElementById("close-form");

  // Ajoutez un gestionnaire d'événement de clic au bouton "Cancel"
  cancelButton.addEventListener("click", function () {
    // Cachez le conteneur de mise à jour lorsque le bouton "Cancel" est cliqué
    document.querySelector(".update-container").style.display = "none";
  });
});
document.addEventListener("DOMContentLoaded", function () {
  // Sélectionnez le bouton "Cancel" par son ID
  const cancelButton = document.getElementById("close-form");

  // Ajoutez un gestionnaire d'événement de clic au bouton "Cancel"
  cancelButton.addEventListener("click", function () {
    // Cachez le conteneur de mise à jour lorsque le bouton "Cancel" est cliqué
    document.querySelector(".add-images").style.display = "none";
  });
});
function openImageForm(productId) {
  // Affichez le formulaire d'ajout d'images
  const updateContainer = document.querySelector(".update-container");
  updateContainer.style.display = "block";

  // Pré-remplissez le formulaire avec l'ID du produit
  const updateForm = updateContainer.querySelector("form");
  updateForm.reset(); // Assurez-vous que le formulaire est réinitialisé
  updateForm.querySelector("[name='update_id']").value = productId;
}
