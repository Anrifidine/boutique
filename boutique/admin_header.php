<?php

include 'connexion.php';
//session_start();
$admin_id = $_SESSION['admin_name'];
if (!isset($admin_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="style.css">

    <title>Document</title>
</head>

<body>
    <header class="header">
        <div class="flex">
            <a href="admin_pannel.php"><img class="logo" src="image/logo.png" alt="LOGO"></a>
            <nav class="navbar">
                <a href="admin_pannel.php">home</a>
                <a href="admin_product.php">products</a>
                <a href="admin_orders.php">others</a>
                <a href="admin_user.php">users</a>
                <a href="admin_message.php">messages</a>
            </nav>
            <div class="icons">
                <i id="user-btn"><span class="material-icons">
                        person
                    </span>
                </i>
                <i id="menu-btn"><span class="material-icons">menu</span></i>


            </div>
            <div class="user-box" id="user-box">
                <p>username : <span><?php echo $_SESSION['admin_name'];  ?></span></p>
                <p>Email : <span><?php echo $_SESSION['admin_email'];  ?></span></p>
                <form method="post">
                    <button type="submit" name="logout" class="logout-btn">log out</button>
                </form>
            </div>
        </div>

    </header>
    <div class="banner">
        <div class="detail">
            <h1>admin dashboard</h1>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. </p>
        </div>

    </div>

    <script src="script.js"></script>








</body>

</html>