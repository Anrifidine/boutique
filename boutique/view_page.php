<?php
include 'connexion.php';

session_start();
$user_id = $_SESSION['user_id'];
if (!isset($user_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}

if (isset($_GET['pid'])) {
    $product_id = $_GET['pid'];
    $select_product = mysqli_query($conn, "SELECT * FROM `products` WHERE id = $product_id") or die('query failed');
    if (mysqli_num_rows($select_product) > 0) {
        $fetch_product = mysqli_fetch_assoc($select_product);
    } else {
        echo 'Produit non trouvé.';
    }
} else {
    echo 'ID de produit non spécifié.';
}

function isProductInWishlist($user_id, $product_id)
{
    global $conn;

    $user_id = mysqli_real_escape_string($conn, $user_id);
    $product_id = mysqli_real_escape_string($conn, $product_id);

    $select_query = "SELECT * FROM productsinwishlist WHERE id_user=$user_id AND id_product=$product_id";
    $result = mysqli_query($conn, $select_query);

    return mysqli_num_rows($result) > 0;
}
function isProductInCart($user_id, $product_id)
{
    global $conn;

    $user_id = mysqli_real_escape_string($conn, $user_id);
    $product_id = mysqli_real_escape_string($conn, $product_id);

    $select_query = "SELECT * FROM produitcart WHERE id_user=$user_id AND id_product=$product_id";
    $result = mysqli_query($conn, $select_query);

    return mysqli_num_rows($result) > 0;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.17.0/font/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
    <title>Détails du Produit</title>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var smallProductImages = document.querySelector(".small-product-images");
            var mainImage = document.querySelector(".left-content img");

            smallProductImages.addEventListener("click", function(event) {
                var clickedImage = event.target.closest(".product-image");
                if (clickedImage) {
                    var newImageSrc = clickedImage.getAttribute("src");
                    console.log("Clicked thumbnail:", newImageSrc); // Add this line for debugging
                    mainImage.setAttribute("src", newImageSrc);
                }
            });
        });
    </script>
</head>

<body>
    <?php include 'header.php'; ?>

    <div class="vide"></div>
    <div class="view">
        <table width="100%">
            <tr>
                <th width="60%">
                    <div class="left-content">
                        <img src="image/<?php echo $fetch_product['image']; ?>" class="card-img-top" alt="<?php echo $fetch_product['name']; ?>">
                    </div>
                </th>
                <th width="40%">
                    <div class="right-content">
                        <h5 class="card-texts"><?php echo $fetch_product['name']; ?></h5>
                        <hr>
                        <p class="card-texts">Prix: $<?php echo $fetch_product['price']; ?></p>

                        <hr>
                        <p class="card-texts">Description: <?php echo $fetch_product['product_detail']; ?></p>

                        <!-- Afficher les images associées au produit -->
                        <?php
                        $product_id = $fetch_product['id'];
                        $is_in_wishlist = isProductInWishlist($user_id, $product_id);
                        $is_in_cart = isProductInCart($user_id, $product_id);
                        $select_images = mysqli_query($conn, "SELECT image FROM product_images WHERE product_id = $product_id") or die('query failed');
                        ?>

                        <?php if (mysqli_num_rows($select_images) > 0) : ?>
                            <div class="small-product-images">

                                <img src="image/<?php echo $fetch_product['image']; ?>" class="product-image" alt="<?php echo $fetch_product['name']; ?>">
                                <?php while ($fetch_images = mysqli_fetch_assoc($select_images)) : ?>
                                    <img src="image/<?php echo $fetch_images['image']; ?>" class="product-image">
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>

                        <form method="post">
                            <input type="number" id="product_quantity_<?php echo $fetch_product['id']; ?>" name="product_quantity" value="1" min="1" placeholder="Quantité">
                            <input type="hidden" name="product_id" value="<?php echo $fetch_product['id']; ?>">
                            <input type="hidden" name="product_name" value="<?php echo $fetch_product['name']; ?>">
                            <input type="hidden" name="product_price" value="<?php echo $fetch_product['price']; ?>">
                            <input type="hidden" name="product_quantity" value="1" min="1">
                            <input type="hidden" name="product_image" value="<?php echo $fetch_product['image']; ?>">
                            <div class="ico icoview">
                                <button id="<?php echo $product_id . 'Love'; ?>" onclick="addToWishList(<?php echo $product_id; ?>)" type="button" name="add_to_wishlist" style="<?php echo $is_in_wishlist ? 'background-color: red;' : ''; ?>">
                                    wishlist
                                </button>
                                <button id="<?php echo $product_id . 'cart'; ?>" onclick="addToCartView(<?php echo $product_id; ?>)" type="button" name="add_to_cart" style="<?php echo $is_in_cart ? 'background-color: yellow;' : ''; ?>">
                                    panier
                                </button>

                            </div>
                        </form>
                    </div>
                </th>
            </tr>
        </table>
    </div>

    <script src="script2.js"></script>

    <?php include 'footer.php'; ?>

</body>

</html>