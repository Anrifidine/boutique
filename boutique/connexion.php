<?php

$conn = mysqli_connect('localhost', 'root', '', 'boutique') or die('connexion echouer');

$options = [
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4", // Set UTF-8 encoding
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // Optional: Set error mode to exception
];
$pdo = new PDO("mysql:host=localhost;dbname=boutique", "root", '', $options);
