<?php
include './connexion.php';
session_start();

$method = $_SERVER["REQUEST_METHOD"];


if ($method == "GET") {

    echo json_encode(["session" => $_SESSION['user_id']]);
}
if ($method == 'POST') {
    $operation = "";
    $data = json_decode(file_get_contents("php://input"), true);

    if (isset($data['id'])) {
        $product_id = $data['id'];
        $user_id = $_SESSION['user_id'];

        if ($user_id) {
            try {
                // Échapper les données pour éviter les injections SQL
                $user_id = mysqli_real_escape_string($conn, $user_id);
                $product_id = mysqli_real_escape_string($conn, $product_id);
                $insert_query = "SELECT * FROM productsinwishlist WHERE id_user=$user_id AND id_product=$product_id";
                $result = mysqli_query($conn, $insert_query);
                if (mysqli_num_rows($result) > 0) {
                    $operation = "delet";
                    $insert_query = "DELETE FROM productsinwishlist WHERE id_user=$user_id AND id_product=$product_id";
                    $result = mysqli_query($conn, $insert_query);
                } else {
                    $operation = "post";

                    $insert_query = "INSERT INTO productsinwishlist (id_user, id_product) VALUES ('$user_id', '$product_id')";
                    $result = mysqli_query($conn, $insert_query);
                }


                // Vérifier si l'insertion a réussi
                if ($result) {
                    // Insertion réussie
                    echo json_encode([
                        'status' => 'success', 'message' => 'Product added to wishlist', 'operation' => $operation
                    ]);
                } else {
                    // Insertion échouée
                    echo json_encode([
                        'status' => 'error', 'message' => 'Failed to add product to wishlist',                        "use" => $user_id, "pro" => $product_id
                    ]);
                }
            } catch (Exception $e) {
                // Gérer les erreurs de base de données
                echo json_encode(['status' => 'error', 'message' => 'Database error']);
            } finally {
                // Fermer la connexion à la base de données
                mysqli_close($conn);
            }
        } else {
            // Utilisateur non connecté
            echo json_encode(['status' => 'error', 'message' => 'User not logged in']);
        }
    } else {
        // Données invalides reçues
        echo json_encode(['status' => 'error', 'message' => 'Invalid data']);
    }
} else {
    // Méthode de requête invalide
    echo json_encode(['status' => 'error', 'message' => 'Invalid request method']);
}
if ($method == "PUT") {
}
if ($method == "DELET") {
}
