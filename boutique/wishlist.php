<?php
include 'connexion.php';
session_start();

$user_id = $_SESSION['user_id'];

if (!isset($user_id)) {
    header('location: login.php');
    exit();
}

if (isset($_POST['remove_from_wishlist'])) {
    $wishlist_id = $_POST['wishlist_id'];
    $remove_query = "DELETE FROM productsinwishlist WHERE id_product = $wishlist_id AND id_user = $user_id";
    mysqli_query($conn, $remove_query) or die('Suppression de la wishlist échouée');
}

$wishlist_query = "SELECT p.id, w.id_user, p.name, p.price, p.image
                    FROM productsinwishlist w
                    JOIN products p ON w.id_product = p.id
                    WHERE w.id_user = $user_id
";
$wishlist_result = mysqli_query($conn, $wishlist_query) or die('Échec de la récupération de la wishlist');
function isProductInCart($user_id, $product_id)
{
    global $conn;

    $user_id = mysqli_real_escape_string($conn, $user_id);
    $product_id = mysqli_real_escape_string($conn, $product_id);

    $select_query = "SELECT * FROM produitcart WHERE id_user=$user_id AND id_product=$product_id";
    $result = mysqli_query($conn, $select_query);

    return mysqli_num_rows($result) > 0;
} ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.17.0/font/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
    <title>Wishlist</title>
</head>

<body>
    <?php include 'header.php'; ?>
    <div class="vide"></div>

    <div class="wishlist-container">
        <h2>Ma Wishlist</h2>

        <?php if (mysqli_num_rows($wishlist_result) > 0) : ?>
            <ul class="wishlist-products">

                <?php while ($product = mysqli_fetch_assoc($wishlist_result)) :
                    $product_id = $product['id'];

                    $is_in_cart = isProductInCart($user_id, $product_id); ?>
                    <li class="wishlist-product">
                        <img src="image/<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>">
                        <div class="product-details">
                            <h3><?php echo $product['name']; ?></h3>
                            <p>Prix: $<?php echo $product['price']; ?></p>
                            <form method="POST" class="remove-form">
                                <input type="hidden" name="wishlist_id" value="<?php echo $product['id']; ?>">
                                <div class="ico">
                                    <button type="submit" name="remove_from_wishlist" class="remove-button" style="background-color: red;"></button>
                                    <button id="<?php $rescart = $product['id'] . 'cart';
                                                echo $rescart; ?>" onclick="addToCart(<?php echo $product['id']; ?>)" type="button" name="add_to_cart" style="<?php echo $is_in_cart ? 'background-color: yellow;' : ''; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-bag" viewBox="0 0 16 16">
                                            <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z" />
                                        </svg></button>
                                </div>
                            </form>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php else : ?>
            <p>Votre wishlist est vide.</p>
        <?php endif; ?>
    </div>
    <script>
        async function addToCart(id) {
            let response = await fetch("http://localhost:3000/cart.api.php", {
                method: "POST",
                body: JSON.stringify({
                    id: id,
                }),
            });
            let json = await response.json();
            console.log(json);
            if (json.operation == "delet") {
                if (document.getElementById(id + "cart") != null) {
                    let coeur = document.getElementById(id + "cart");

                    coeur.style.backgroundColor = "white";
                }
            } else {
                if (document.getElementById(id + "cart") != null) {
                    let coeur = document.getElementById(id + "cart");

                    coeur.style.backgroundColor = "yellow";
                }
            }
        }
    </script>
    <?php include 'footer.php'; ?>
</body>

</html>