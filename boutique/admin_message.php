<?php

include 'connexion.php';
session_start();
$admin_id = $_SESSION['admin_name'];
if (!isset($admin_id)) {
    header('location:login.php');
}
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:login.php');
    exit();
}
if (isset($_POST['add_product'])) {
    $product_name = mysqli_real_escape_string($conn, $_POST['name']);
    $product_price = mysqli_real_escape_string($conn, $_POST['price']);
    $product_detail = mysqli_real_escape_string($conn, $_POST['detail']);
    $image = $_FILES['image']['name'];
    $image_size = $_FILES['image']['size'];
    $image_tmp_name = $_FILES['image']['tmp_name'];
    $image_folder = 'image/' . $image;

    $select_product_name = mysqli_query($conn, "SELECT name FROM `products` WHERE name = '$product_name'") or die('query faild');
    if (mysqli_num_rows($select_product_name) > 0) {
        $message[] = 'Le nom de produit exsiste deja';
    } else {
        $insert_product = mysqli_query($conn, "INSERT INTO `products` (`name`, `price`, `product_detail`, `image`) VALUES ('$product_name', '$product_price', '$product_detail', '$image')") or die('query failed');
        if ($insert_product) {
            if ($image_size > 2000000) {
                $message[] = "l'image est trop grande";
            } else {
                move_uploaded_file($image_tmp_name, $image_folder);
                $message[] = 'produit ajouter avec succes';
            }
        }
    }
}
if (isset($_GET['delete'])) {
    $delete_id = $_GET['delete'];

    mysqli_query($conn, "DELETE FROM `message` WHERE id = '$delete_id'") or die('query failed3');

    header('location:admin_message.php');
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css">
    <link rel="stylesheet" href="style.css">
    <title>admin pannel</title>
</head>

<body>
    <?php include 'admin_header.php'; ?>
    <?php
    if (isset($message)) {
        foreach ($message as $message) {
            echo '<div class="message">
                            <span>' . $message . '</span>
                            <i class="bi bi-x-circle" onclick="this.parentElement.remove()"><span class="material-icons">
                            clear
                            </span></i>
                        </div>';
        }
    }
    ?>

    <section class="message-container">
        <h1 class="title">message non vu</h1>
        <div class="box-container">
            <?php
            $select_message = mysqli_query($conn, "SELECT * FROM `message`") or die('query failed');
            if (mysqli_num_rows($select_message) > 0) {
                while ($fech_message = mysqli_fetch_assoc($select_message)) {
            ?>
                    <div class="box">
                        <p>user id: <span><?php echo $fech_message['user_id']; ?></span></p>
                        <p>name: <span><?php echo $fech_message['name']; ?></span></p>
                        <p>email: <span><?php echo $fech_message['email']; ?></span></p>
                        <p>tel: <span><?php echo $fech_message['number']; ?></span></p>
                        <p><?php echo $fech_message['message'] ?></p>
                        <a href="admin_message.php?delete=<?php echo $fech_message['id']; ?>;" class="delete" onclick="return confirm('supprimer ce message');">supprimer</a>



                    </div>
            <?php
                }
            } else {
                echo '<div class="empty">
                        <p>pas de message ajouté</p>
                        </div>';
            }

            ?>

        </div>

    </section>


    <script type="text/javasript" src="script.js"></script>

</body>

</html>