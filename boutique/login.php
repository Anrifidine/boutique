<?php
include 'connexion.php';
session_start();


if (isset($_POST['submit-btn'])) {

    $filter_email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
    $email = mysqli_real_escape_string($conn, $filter_email);
    $filter_password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
    $password = mysqli_real_escape_string($conn, $filter_password);

    $select_user = mysqli_query($conn, "SELECT * FROM `users` WHERE email = '$email' AND password='$password'") or die('query failed');
    if (mysqli_num_rows($select_user) > 0) {
        $row = mysqli_fetch_assoc($select_user);
        if ($row['user_type'] == 'admin') {
            $_SESSION['admin_name'] = $row['name'];
            $_SESSION['admin_email'] = $row['email'];
            $_SESSION['admin_id'] = $row['id'];
            header('Location:admin_pannel.php');
        } else if ($row['user_type'] == 'user') {
            $_SESSION['user_name'] = $row['name'];
            $_SESSION['user_email'] = $row['email'];
            $_SESSION['user_id'] = $row['id'];
            header('Location:homeshop.php');
        } else {
            $message[] = ' email ou mot de passe incorrect';
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="style.css">
    <title>connexion</title>
</head>

<body>


    <section class="form-container">
        <?php
        if (isset($message)) {
            foreach ($message as $message) {
                echo '<div class="message">
                            <span>' . $message . '</span>
                            <i class="bi bi-x-circle" onclick="this.parentElement.remove()"><span class="material-icons">
                            clear
                            </span></i>
                        </div>';
            }
        }
        ?>
        <form method="post">
            <h1>se conneter</h1>
            <div class="input-field">
                <label>ton email</label><br>
                <input type="email" name="email" placeholder="entrez votre email" required>
            </div>
            <div class="input-field">
                <label>ton mot de passe</label><br>
                <input type="password" name="password" placeholder="entrez votre mot de passe" required>
            </div>

            <input type="submit" name="submit-btn" value="connexion " class="btn">
            <p>vous n'avez pas de compte ?<a href="register.php">inscription</a></p>


        </form>

    </section>

</body>

</html>